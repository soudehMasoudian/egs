package net.soudeh.egs.config;

import net.soudeh.egs.model.ProductUser;
import net.soudeh.egs.model.UserRole;
import net.soudeh.egs.repository.RoleRepository;
import net.soudeh.egs.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class SetupDataLoader implements
        ApplicationListener<ContextRefreshedEvent> {

    boolean alreadySetup = false;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (alreadySetup)
            return;

        UserRole adminRole = roleRepository.findByRole("ROLE_ADMIN");
        ProductUser user = new ProductUser();
        user.setFirstName("Test");
        user.setLastName("Test");
        user.setPassword(passwordEncoder.encode("test"));
        user.setEmail("test@test.com");
        user.setRoles(Arrays.asList(adminRole));
        user.setActive(true);
        userRepository.save(user);

        alreadySetup = true;
    }

    @Transactional
    UserRole createRoleIfNotFound(
            String name) {

        UserRole role = roleRepository.findByRole(name);
        if (role == null) {
            role = new UserRole(name);
            roleRepository.save(role);
        }
        return role;
    }

}
