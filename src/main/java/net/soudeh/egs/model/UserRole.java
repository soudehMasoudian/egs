package net.soudeh.egs.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.Collection;

@Data
@Entity
public class UserRole extends AbstractEntity{
    private long userId;

    private String role;

    @ManyToMany(mappedBy = "roles")
    private Collection<ProductUser> users;

    public UserRole(String role) {
        this.role = role;
    }

    public UserRole() {

    }
}
