package net.soudeh.egs.model;

import lombok.Data;
import net.soudeh.egs.utils.RateEnum;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Data
public class Rate extends AbstractEntity {

    private long userId;

    private long productId;

    @Column
    private RateEnum rate;
}
