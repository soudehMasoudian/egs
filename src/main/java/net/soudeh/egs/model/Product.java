package net.soudeh.egs.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
@Data
public class Product extends AbstractEntity{

    private long categoryId;

    @Column
    private String title;

    @Column
    private String description;

    @Column
    private BigDecimal price;

}
