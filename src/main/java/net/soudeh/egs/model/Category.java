package net.soudeh.egs.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Data
public class Category extends AbstractEntity {

    @Column
    private String name;
}
