package net.soudeh.egs.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Data
public class Comment extends AbstractEntity{

    private long userId;

    private long productId;

    @Column
    private String comment;
}
