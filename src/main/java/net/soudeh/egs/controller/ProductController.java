package net.soudeh.egs.controller;


import net.soudeh.egs.dto.BaseResponse;
import net.soudeh.egs.model.Product;
import net.soudeh.egs.service.ProductService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

@RestController
@RequestMapping(value = "/product", produces = "application/json")
public class ProductController {

    private static final Log logger = LogFactory.getLog(ProductController.class);

    @Autowired
    ProductService productService;

    @ExceptionHandler(Exception.class)
    public ResponseEntity<BaseResponse> handleError(HttpServletRequest req, Exception exception) {
        if (exception instanceof IllegalArgumentException) {
            logger.info("IllegalArgumentException Request[" + req.getRequestURI() + "] User[" + req.getUserPrincipal().getName() + "] Response[" + exception.getMessage() + "]");
            return new ResponseEntity(new BaseResponse(exception.getMessage(),
                    exception.getMessage(), null),
                    HttpStatus.NOT_ACCEPTABLE);
        } else if (exception instanceof HttpMediaTypeNotSupportedException) {
            logger.info("HttpMediaTypeNotSupportedException Request[" + req.getRequestURI() + "] User[" + req.getUserPrincipal().getName() + "] Response[" + exception.getMessage() + "]");
            return new ResponseEntity<>(new BaseResponse(exception.getMessage(),
                    exception.getMessage(), null),
                    HttpStatus.UNSUPPORTED_MEDIA_TYPE);
        } else if (exception instanceof HttpClientErrorException.Unauthorized) {
            logger.error("Exception Request[" + req.getRequestURI() + "] User[" + req.getUserPrincipal().getName() + "] raised " + exception, exception);
            exception.getMessage();
        }
        logger.error("Exception Request[" + req.getRequestURI() + "] User[" + req.getUserPrincipal().getName() + "] raised " + exception, exception);
        return new ResponseEntity<>(new BaseResponse("internal.error",
                exception.getMessage(),
                exception.getMessage(), null),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PostMapping(value = "/create")
    public ResponseEntity<Product> createProduct(@RequestBody @Valid Product product) {
        Product result = productService.createProduct(product);
        return ResponseEntity.ok(result);
    }
}
