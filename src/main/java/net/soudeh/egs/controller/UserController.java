package net.soudeh.egs.controller;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.soudeh.egs.message.RestResponse;
import net.soudeh.egs.model.ProductUser;
import net.soudeh.egs.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.*;

import javax.security.sasl.AuthenticationException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    UserService userService;

    private static final Log logger = LogFactory.getLog(ProductController.class);

    @PostMapping(value = "/register")
    public @ResponseBody
    RestResponse registerUser(@RequestBody ProductUser user) throws AuthenticationException, JSONException {
        ProductUser optionalUser = userService.findUserByUsername(user.getUsername());
        // or userService.findUserByName(username);
        if (optionalUser != null) {
            return getBadRequestResponse(String.format("User with name : %s already exists", user.getUsername()));
        } else {
            userService.registerNewUserAccount(user);
            return getSuccessfulResponse("user registered successfully");
        }
    }

   protected RestResponse getSuccessfulResponse(String message) {
        return new RestResponse(HttpStatus.OK, message);
    }

    protected RestResponse getBadRequestResponse(String message) {
        return new RestResponse(HttpStatus.BAD_REQUEST, message);
    }

    @PostMapping(value = "/login")
    public ProductUser loginUser(@RequestParam("user") String username, @RequestParam("password") String password) {
        String token = getJwtToken(username);
        ProductUser user = new ProductUser();
        user.setUsername(username);
        user.setToken(token);
        return user;
    }

    private String getJwtToken(String username) {
        String secretKey = "mySecretKey";
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                .setId("myJwtId")
                .setSubject(username)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 600000))
                .signWith(SignatureAlgorithm.HS512,
                        secretKey.getBytes()).compact();

        return "Bearer " + token;




    }
}
