package net.soudeh.egs.repository;

import net.soudeh.egs.model.ProductUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<ProductUser, Long> {
    List<ProductUser> findAll();
    ProductUser saveAndFlush(ProductUser product);
    ProductUser findByUsername(String username);
}
