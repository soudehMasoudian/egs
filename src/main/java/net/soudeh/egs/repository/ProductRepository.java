package net.soudeh.egs.repository;

import net.soudeh.egs.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
//    Product getById(long id);
    List<Product> findAll();
//    List<Product> findByName(String name);
    Product saveAndFlush(Product product);
}
