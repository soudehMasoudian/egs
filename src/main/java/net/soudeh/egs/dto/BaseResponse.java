package net.soudeh.egs.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class BaseResponse {
    private String code;
    private String messageFa;
    private String messageEn;
    private String trace;
    @JsonIgnore
    private String mobileNo;
    @JsonIgnore
    private Long userId;

    public BaseResponse() {
    }

    public BaseResponse(final String code, final String messageFa, final String messageEn) {
        this.code = code;
        this.messageFa = messageFa;
        this.messageEn = messageEn;
    }

    public BaseResponse(final String code, final String messageFa, final String messageEn, final String trace) {
        this.code = code;
        this.messageFa = messageFa;
        this.messageEn = messageEn;
        this.trace = trace;
    }
}
