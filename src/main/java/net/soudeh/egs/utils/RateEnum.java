package net.soudeh.egs.utils;

public enum RateEnum {
    EXCELLENT,
    GOOD,
    AVERAGE,
    BAD,
    VERY_BAD;
}
