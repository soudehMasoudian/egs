package net.soudeh.egs.message;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class RestResponse {
    private int statusCode;
    private String reasonPhrase;
    private String detailMessage;

    public RestResponse(HttpStatus status, String detailMessage) {
        statusCode = status.value();
        reasonPhrase = status.getReasonPhrase();
        this.detailMessage = detailMessage;
    }

    public RestResponse of(HttpStatus status) {
        return of(status, null);
    }

    public RestResponse of(HttpStatus status, Exception ex) {
        return new RestResponse(status, ex.getMessage());
    }

}
