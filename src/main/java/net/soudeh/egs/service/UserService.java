package net.soudeh.egs.service;

import net.soudeh.egs.model.ProductUser;
import net.soudeh.egs.repository.RoleRepository;
import net.soudeh.egs.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.security.sasl.AuthenticationException;
import javax.transaction.Transactional;
import java.util.*;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Transactional
    public ProductUser registerNewUserAccount(ProductUser user) throws AuthenticationException {

        if (userExist(user.getUsername())) {
            throw new AuthenticationException(
                    ("There is an account with this username: " + user.getUsername()));
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        user.setRoles(Arrays.asList(roleRepository.findByRole("ROLE_USER")));
        return userRepository.save(user);
    }

    public boolean userExist (String username) {
        ProductUser user = userRepository.findByUsername(username);
        if(user != null) {
            return true;
        }
        return false;
    }

    @Transactional
    public Optional<ProductUser> findUserById(Long id) {
        return userRepository.findById(id);
    }

    @Transactional
    public ProductUser findUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

}
