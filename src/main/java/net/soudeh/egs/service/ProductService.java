package net.soudeh.egs.service;

import net.soudeh.egs.model.Product;
import net.soudeh.egs.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    @Transactional
    public Product createProduct(Product product) {
        return productRepository.saveAndFlush(product);
    }
}
